## shrpcom

Pronounced "sharp-comm". Short for "Shell-based Reverse Polish Compiler". 
Is an experimental programming language which accepts 
function arguments in reverse-polish form, and compiles to C. Almost no real
documentation written so far-- just comments in the code. More functionality is 
yet to be developed, but currently the language supports:

* Addition
* Subtraction
* Multiplication
* Division
* Modulus
* Text-replacement macros
* CLI Input/Output (very rudimentary, limited use)
* Functions
* Recursion
* Conditionals (kind of)

Things the language doesn't support:

* File handling
* Loops
* Strings
* Syntax that sane people might want to use
* Anything not on the list of features above

Licensed under the GNU GPLv3. See COPYING for details.
