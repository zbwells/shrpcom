###############
### This file is part of "shrpcom".
### Shrpcom is free software: you can redistribute it and/or modify it 
### under the terms of the GNU General Public License as published by 
### the Free Software Foundation, either version 3 of the License, or 
### (at your option) any later version.

### Shrpcom is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of 
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with shrpcom. If not, see <https://www.gnu.org/licenses/>.
### Copyright 2023 Zander B. Wells <zbwells@protonmail.com>
###############

# meant to be used with shrpcom.sh

# Populate an associative array with the defined macros in the file
populate_macros()
{
    text=$(cat $FILE)
    currentmacro=""
    
    for word in $text; do
        head=$(echo $word | head -c 1)
        tail=$(echo $word | tail -c +2)

        if test "$head" == "#"; then
            currentmacro="$tail"
        elif test "$head" == ":"; then
            currentmacro=""
        elif test -z $currentmacro; then
            continue
        elif test "$head" == "@"; then
            MACROS[$currentmacro]="${MACROS[$currentmacro]} ${MACROS[$tail]}"
        else
            MACROS[$currentmacro]="${MACROS[$currentmacro]} $word"
        fi
    done
}

# Take the body of the program and expand out all the macros
# also, remove comments
expand_macros()
{   
    body=$(cat $FILE | grep -v "#\|;;")    
    newbody=""

    for word in $body; do
        head=$(echo $word | head -c 1)
        tail=$(echo $word | tail -c +2)

        if test "$head" == "@"; then
            newbody="$newbody ${MACROS[$tail]}"
        elif test "$word" == ":"; then
            newbody="$newbody $word\n"
        else
            newbody="$newbody $word"
        fi
    done

    echo -e "${newbody}"
}

# run both prior functions
process_macros()
{
    FILE=$1
    declare -A MACROS

    set -o noglob
    populate_macros
    expand_macros   
    set +o noglob 
}


