###############
### This file is part of "shrpcom".
### Shrpcom is free software: you can redistribute it and/or modify it 
### under the terms of the GNU General Public License as published by 
### the Free Software Foundation, either version 3 of the License, or 
### (at your option) any later version.

### Shrpcom is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of 
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with shrpcom. If not, see <https://www.gnu.org/licenses/>.
### Copyright 2023 Zander B. Wells <zbwells@protonmail.com>
###############

# functions to go along with shrpcom.sh

### VARIABLES ###
# $COUNT is a global counter variable from shrpcom.sh
# $OUT is the name of the file passed to shrpcom.sh
#################

# Generate code to add the previous two numbers
add()
{
    COUNT=$((COUNT-2))
    echo "  _$COUNT = _$COUNT + _$((COUNT+1));" >> $OUT
}

# Generate code to subtract the previous two numbesr
sub()
{
    COUNT=$((COUNT-2))
    echo "  _$COUNT = _$COUNT - _$((COUNT+1));" >> $OUT
}

# Generate code to multiply the previous two numbers together
mul()
{
    COUNT=$((COUNT-2))
    echo "  _$COUNT = _$COUNT * _$((COUNT+1));" >> $OUT
}

# Generate code to divide the previous two numbers
div()
{
    COUNT=$((COUNT-2))
    echo "  _$COUNT = _$COUNT / _$((COUNT+1));" >> $OUT
}

# Generate code to raise the first number to the power of the next
pow()
{
    COUNT=$((COUNT-2))
    echo "  _$COUNT = pow(_$COUNT, _$((COUNT+1)));" >> $OUT
}

# Generate code to perform modulo arithmetic on two values
mod()
{
    COUNT=$((COUNT-2))
    echo "  _$COUNT = _$COUNT % _$((COUNT+1));" >> $OUT    
}

# Generate code to print out 'top of stack' essentially
print()
{
    echo >> $OUT
    echo "  printf(\"%g\\n\", _$((COUNT-1)));" >> $OUT
}

# Generate code to print out a newline
newline()
{
    echo >> $OUT
    echo "  putchar('\n');" >> $OUT
}

# Generate code to perform square root on top of stack
sqrt()
{
    COUNT=$((COUNT-1))
    echo "  _$COUNT = sqrt(_$COUNT);" >> $OUT
}

# Generate code to put pi on stack
pi()
{
    echo "  _$COUNT = M_PI;" >> $OUT
}

# Prepare to perform a reducing operation across a sequence in reverse
foldr()
{
    echo -n "  for (_$COUNT = _$((COUNT-3));" >> $OUT 
    echo " _$COUNT >= _$((COUNT-2)); _$COUNT--) {" >> $OUT
    echo -n "  " >> $OUT
}

# Prepare to perform a reducing operation across a sequence 
foldl()
{
    echo -n "  for (_$COUNT = _$((COUNT-3));" >> $OUT 
    echo " _$COUNT <= _$((COUNT-2)); _$COUNT++) {" >> $OUT
    echo -n "  " >> $OUT
}

# Generate cord to end the fold operation
endfold()
{
    COUNT=$((COUNT-3))
    echo "  }" >> $OUT
    echo "  _$COUNT = _$((COUNT+2));" >> $OUT
}

# Conditional logic //TODO: TEST IN MORNING AFTER TAKING OUT TRASH
ifeq()
{
    COUNT=$((COUNT-2))
    echo "  if (_$COUNT == _$((COUNT+1))) {" >> $OUT
}

ifneq()
{
    COUNT=$((COUNT-2))
    echo "  if (_$COUNT != _$((COUNT+1))) {" >> $OUT
}

ifles()
{
    COUNT=$((COUNT-2))
    echo "  if (_$COUNT < _$((COUNT+1))) {" >> $OUT    
}

ifgre()
{
    COUNT=$((COUNT-2))
    echo "  if (_$COUNT > _$((COUNT+1))) {" >> $OUT   
}

endif()
{
    COUNT=$((COUNT-1))
    echo "  }" >> $OUT
}

quit()
{
    echo "  exit(0);" >> $OUT
}

clear()
{
    COUNT=0
}

# Generate code to emulate pushing the object onto the stack
push()
{
    if test $(echo $word | head -c 1) == "_"; then
        echo "  _$COUNT = -$(echo $word | tail -c +2);" >> $OUT
    else
        echo "  _$COUNT = $word;" >> $OUT
    fi
}

# Generate code to return from the function (possibly to be removed later)
ret()
{
    COUNT=$((COUNT-1))
    echo "  return _$COUNT;" >> $OUT
}
